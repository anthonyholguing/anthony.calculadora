#include <iostream>
using namespace std;
int main()
{
	int seleccion = 0;
	short numero1 = 0, numero2 = 0;
	short resultado;
	while (seleccion != 5)
	{
		cout << "******************************************\n";
		cout << "*NOMBRE: ANTHONY HOLGUIN                  *\n";
		cout << "*ORGANIZACION Y AQUITECTURA DEL COMPUTADOR*\n";
		cout << "*DOCENTE: ING. MARCO AYOVI                *\n";
		cout << "*******BIENVENIDO AL SISTEMA**************\n";
		cout << "***************CALCULADORA****************\n";
		cout << "INGRESE UN NUMERO DEL MENU\n";
		cout << "\n1) ------SUMA\n";
		cout <<   "2) ------RESTA\n";
		cout <<   "3) ------MULTIPLICACION\n";
		cout <<   "4) ------DIVISION\n";
		cout <<   "5) ------SALIR\n"; cin >> seleccion;
		switch (seleccion)
		{
		case 1: cout << "\n Usted ha seleccionado la opcion 1: 'Suma'\n";
			cout << "Ingrese el primero numero:\n"; cin >> numero1;
			cout << "Ingrese el segundo numero:\n"; cin >> numero2;
			_asm
			{
				mov ax, numero1;
				mov bx, numero2;
				add ax, bx;
				mov resultado, ax;
			}
			printf("El resultado es: %d \n", resultado);
			printf("\n--------------------------------\n");
			break;
		case 2: cout << "\n Usted ha selecionado la opcion 2: 'RESTA'\n";
			cout << "Ingrese el PRIMER numero:\n"; cin >> numero1;
			cout << "Ingrese el SEGUNDO numero:\n"; cin >> numero2;
			_asm
			{
				mov ax, numero1;
				mov bx, numero2;
				sub ax, bx;
				mov resultado, ax;
			}
			printf("El resultado es: %d \n", resultado);
			break;
		case 3: cout << "\n Usted ha selecionado la opcion 3: 'Multiplicacion'\n";
			cout << "Ingrese el Primer numero:\n"; cin >> numero1;
			cout << "Ingrese el Segundo numero:\n"; cin >> numero2;
			_asm
			{
				mov ax, numero1;
				mov bx, numero2;
				mul bx;
				mov resultado, ax;
			}
			printf("El resultado es: %d \n", resultado);
			break;
		case 4: cout << "\n Usted ha seleccionado la opcion 4: 'DIVISION'\n";
			cout << "Ingrese el Primer numero:\n"; cin >> numero1;
			cout << "Ingrese el Segundo numero:\n"; cin >> numero2;
			_asm
			{
				xor dx, dx;
				mov ax, numero1;
				mov bx, numero2;
				div bx;
				mov resultado, ax;
			}
			printf("El resultado es: %d \n", resultado);
			break;
		case 5: cout << "\n Usted ha salido del programa'\n";
			break;
		default: cout << "Usted ha ingresado una opcion incorrecta";
		}
	}
	return 0;
}